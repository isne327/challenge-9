#include <iostream>
#include <list>

using namespace std;

void construct(int n,list<int> &player);
void run(int m, list<int> player,int n);

int main ()
{
	list<int> player;
	int n,m;
	cout << "Please input the number of the player : ";
	cin >> n;
	cout << "Please input the number of passes : ";
	cin >> m;
	construct(n,player); //Create list of player
	run(m,player,n); //Calculate and display
}

void construct(int n,list<int> &player) //Use to Create a list of player (1 to n)
{
	for(int i=1 ; i<=n ; i++)
	{
		player.push_back(i);
	}
}

void run(int m, list<int> player , int n) //Calculation and display program
{
	list<int>::iterator it=player.begin();
	list<int>::iterator hold;
	int r,count=0;
	
	while(player.size() != 1) //Do until 1 player left
	{		
		for(int i=1 ; i<=m ; i++) //Pass the pointer m times
		{
			if(*it == player.back() && i==m)//if the last loop point at the last element do nothing
			{
			}
			if(*it == player.back()) //if poiter point at the last element skip it to element1 instead of ++
			{
				it = player.begin();
			}
			else
			{
				it++;	
			}	
		}
		
		cout << "Player " << *it << " Eleminated"<<endl;
		
		if(*it == player.back()) //Erase the last element and change pointer to point the the first element
		{
			player.erase(it);
			it = player.begin();
		}
		else //Erase every elements exept the last one		
		it = player.erase(it);	
	}
	cout << endl << "**********************************" <<endl;
	cout << "WINNER IS PLAYER : " << *it;	
}
